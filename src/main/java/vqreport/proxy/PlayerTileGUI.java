package vqreport.proxy;

import java.util.ArrayList;
import java.util.List;

import lotr.common.LOTRMod;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ContainerChest;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import vqreport.core.AOSTileData;
import vqreport.core.TileEnum;

public class PlayerTileGUI extends ContainerChest {

	public String mode;
	
	public PlayerTileGUI(IInventory chest, IInventory player) {
		super(chest, player);
		System.out.println("test");
		
		chest.setInventorySlotContents(5, new ItemStack(LOTRMod.bodyArnor));
		
	}
	
	@Override
	public boolean canInteractWith(EntityPlayer var1) {
		return true;
	}
	
	@Override
	public void onContainerClosed(EntityPlayer player) {
	super.onContainerClosed(player);

	}
	@Override
    public ItemStack slotClick(int slot, int p_75144_2_, int p_75144_3_, EntityPlayer player) {
		if(slot == 0) {
			for(TileEnum enums : TileEnum.values()) {
				AOSTileData data = AOSTileData.of(enums);
				if(data.owner == player.getUniqueID() && data.claimedTaxes == false) {
					data.claimedTaxes = true;
					data.claimedInfo = "claimedWithTax";
					for(ItemStack stacks : enums.reward.rewardlist) {
						int taxStatus = data.tax;
						int newStackSice = taxCalculator(stacks.stackSize, data.tax);
		        		ItemStack stacktje = new ItemStack(stacks.getItem(), newStackSice);
		        		player.inventory.addItemStackToInventory(stacktje);
					}
					data.markDirty();
				}
			}
			
		} 
		if(slot == 1) {
			for(TileEnum enums : TileEnum.values()) {
				AOSTileData data = AOSTileData.of(enums);
				if(data.owner == player.getUniqueID() && data.claimedTaxes == false) {
					data.claimedTaxes = true;
					data.claimedInfo = "claimedNoTax";
					for(ItemStack stacks : enums.reward.rewardlist) {
		        		ItemStack stacktje = new ItemStack(stacks.getItem(), stacks.stackSize);
		        		player.inventory.addItemStackToInventory(stacktje);
					}
					data.markDirty();
				}
			}
		} 
		return null;
    }
	
	public static int taxCalculator(int stack, int tax) {
		if(tax == 0) {
			return stack;
		}
		if(tax == 1) {
			System.out.println("test");
			float taxAmount = (11 * stack / 100);
			return stack - (int)taxAmount;
		}
		if(tax == 2) {
			float taxAmount = (22 * stack / 100);
			return stack - (int)taxAmount;
			
		}
		if(tax == 3) {
			float taxAmount = (33 * stack / 100);
			return stack - (int)taxAmount;
			
		}
		if(tax == 4) {
			float taxAmount = (44 * stack / 100);
			return stack - (int)taxAmount;
			
		}
		if(tax == 5) {
			float taxAmount = (55 * stack / 100);
			return stack - (int)taxAmount;
			
		}
		if(tax == 6) {
			float taxAmount = (66 * stack / 100);
			return stack - (int)taxAmount;
			
		}
		if(tax == 7) {
			float taxAmount = (77 * stack / 100);
			return stack - (int)taxAmount;
			
		}
		if(tax == 8) {
			float taxAmount = (88 * stack / 100);
			return stack - (int)taxAmount;
			
		}
		if(tax == 9) {
			float taxAmount = (100 * stack / 100);
			return stack - (int)taxAmount;
			
		}
		
		return tax;
		
	}
}