package vqreport.proxy;

import lotr.common.LOTRMod;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.ContainerChest;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryBasic;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.server.S2DPacketOpenWindow;
import vqreport.core.AOSTileData;
import vqreport.core.TileEnum;

public class PlayerTileTaxesGUI extends ContainerChest {

	public String mode;
	
	public PlayerTileTaxesGUI(IInventory chest, IInventory player) {
		super(chest, player);
		System.out.println("test");
		
		chest.setInventorySlotContents(5, new ItemStack(LOTRMod.bodyArnor));
		
	}
	
	@Override
	public boolean canInteractWith(EntityPlayer var1) {
		return true;
	}
	
	@Override
	public void onContainerClosed(EntityPlayer player) {
	super.onContainerClosed(player);

	}
	@Override
    public ItemStack slotClick(int p_75144_1_, int p_75144_2_, int p_75144_3_, EntityPlayer playerSP) {
		if(this.getSlot(p_75144_1_) != null || p_75144_1_ != 0) {
			if(this.getSlot(p_75144_1_).getStack() != null) {	
			String tileName = this.getSlot(p_75144_1_).getStack().getDisplayName();
		    	if(TileEnum.forName(tileName) != null) {
		    	TileEnum tiles = TileEnum.forName(tileName);
					if(AOSTileData.of(tiles ) != null) {	
						
						AOSTileData data = AOSTileData.of(tiles);
						EntityPlayerMP player = (EntityPlayerMP) playerSP;	
						player.closeScreen();

			        	if (player.openContainer != player.inventoryContainer) {
			                player.closeScreen();
			            }
			            player.getNextWindowId();
			            final InventoryBasic chest = new InventoryBasic("Taxes Decide", true, 9);
			            chest.setInventorySlotContents(0, new ItemStack(Blocks.wool, 1,5 ).setStackDisplayName("Claim"));
			            chest.setInventorySlotContents(1, new ItemStack(Blocks.wool, 1,14).setStackDisplayName("Cancel"));
			
			            player.playerNetServerHandler.sendPacket(new S2DPacketOpenWindow(player.currentWindowId, 0, "Taxes Decide", 9, true));
			            player.openContainer = new PlayerTileTaxesGUIDecide(player.inventory, chest, tiles);
			            player.openContainer.windowId = player.currentWindowId;
			            player.openContainer.addCraftingToCrafters(player);
			            
					}
		    	}
			}
		} 
		return null;
    }

}