package vqreport.proxy;

import java.util.UUID;

import lotr.common.LOTRMod;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ContainerChest;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryBasic;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.server.S2DPacketOpenWindow;
import vqreport.core.AOSTileData;
import vqreport.core.TileEnum;

public class PlayerTileTaxesGUIDecide extends ContainerChest {

	public String mode;
	public TileEnum enums;
	public PlayerTileTaxesGUIDecide(IInventory chest, IInventory player, TileEnum tiles) {
		super(chest, player);
		this.enums = tiles;
	}
	
	@Override
	public boolean canInteractWith(EntityPlayer var1) {
		return true;
	}
	
	@Override
	public void onContainerClosed(EntityPlayer player) {
	super.onContainerClosed(player);

	}
	@Override
    public ItemStack slotClick(int slot, int p_75144_2_, int p_75144_3_, EntityPlayer playerSP) {
		System.out.println("test");
		if(slot == 0) {
			System.out.println("test");

		}
		if(slot == 1) {
			System.out.println("test 2");
			EntityPlayerMP player = (EntityPlayerMP) playerSP;
        	if (player.openContainer != player.inventoryContainer) {
                player.closeScreen();
            }
            player.getNextWindowId();
            final InventoryBasic chest = new InventoryBasic("Leader Taxes", true, 54);
            int slotnumber = 0;
            for(TileEnum tiles : TileEnum.values()) {
            	AOSTileData data = AOSTileData.of(tiles);
            	for(UUID uuid : data.factionManagement) {
            		if(uuid != null) {
            			if(slotnumber >= 53) continue;
		                chest.setInventorySlotContents(slotnumber, new ItemStack(LOTRMod.bodyArnor).setStackDisplayName(tiles.name));
		                slotnumber++;
            			
            		}
            	}
            }

            player.playerNetServerHandler.sendPacket(new S2DPacketOpenWindow(player.currentWindowId, 0, "Leader Taxes", 54, true));
            player.openContainer = new PlayerTileTaxesGUI(player.inventory, chest);
            player.openContainer.windowId = player.currentWindowId;
            player.openContainer.addCraftingToCrafters(player);
		}
		return null;
    }

}