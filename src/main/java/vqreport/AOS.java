package vqreport;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.event.FMLServerStoppedEvent;
import cpw.mods.fml.common.event.FMLServerStoppingEvent;
import net.minecraft.command.ICommand;
import net.minecraftforge.common.config.Configuration;
import vqreport.commands.CommandAddSiege;
import vqreport.commands.CommandAddSiegeConfiguration;
import vqreport.commands.CommandTileManagementFactionManagement;
import vqreport.commands.CommandTileManagementFactionManagementBoost;
import vqreport.commands.CommandTileManagementFactionManagementTaxes;
import vqreport.commands.CommandTileManagementPlayer;
import vqreport.commands.CommandTilePlayerGUI;
import vqreport.commands.CommandTilePlayerGUILeader;
import vqreport.core.AOSTileData;

@Mod(modid = AOS.MODID, name = AOS.MODNAME, version = AOS.VERSION, acceptableRemoteVersions ="*")
public class AOS {


	public static final String MODID = "aossiege";
	public static final String MODNAME= "aossiege";
	public static final String VERSION = "1.3";


    
    public AOSEvents skillEvents;
    public static Configuration configFile;
    

    @Mod.EventHandler
    public void init(final FMLInitializationEvent event) {
    	skillEvents = new AOSEvents();

    }
    @Mod.EventHandler
    public void preInit(final FMLPreInitializationEvent event) { 
    	AOSConfig.setupAndLoad(event); 
		
    }
	@Mod.EventHandler
    public void onServerStarting(final FMLServerStartingEvent event) {
		AOSTileData.setRoots();
        event.registerServerCommand((ICommand)new CommandAddSiegeConfiguration());
        event.registerServerCommand((ICommand)new CommandAddSiege());
       event.registerServerCommand(new CommandTileManagementPlayer());
        event.registerServerCommand(new CommandTileManagementFactionManagement());
        event.registerServerCommand(new CommandTilePlayerGUI());
        event.registerServerCommand(new CommandTileManagementFactionManagementTaxes());
        event.registerServerCommand(new CommandTileManagementFactionManagementBoost());
        event.registerServerCommand(new CommandTilePlayerGUILeader());
        
        AOSTileData.runOnce();
    }

	@Mod.EventHandler
    public void onServerDone(final FMLServerStoppingEvent event) {
		AOSTileData.quit();

    }
    @Mod.EventHandler
    public void onServerDone(final FMLServerStoppedEvent event) {
		AOSTileData.quit();


    }
}
