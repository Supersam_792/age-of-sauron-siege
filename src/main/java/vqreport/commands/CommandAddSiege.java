package vqreport.commands;

import java.util.Arrays;
import java.util.List;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.play.server.S3EPacketTeams;
import net.minecraft.scoreboard.ScorePlayerTeam;
import net.minecraft.scoreboard.Scoreboard;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import vqreport.AOSConfig;
import vqreport.AOSEvents;

public class CommandAddSiege extends CommandBase {
	public static AOSEvents event;
    public String getCommandName() {
        return "battle";
    }
    
    @Override
    public int getRequiredPermissionLevel() {
        return 0;
    }
    public String getCommandUsage(final ICommandSender sender) {
        return "commands.lotrta.loreadds.usage";
    }

    protected Scoreboard getscoreboard() {
        return MinecraftServer.getServer().worldServerForDimension(0).getScoreboard();
    }
    
    public void processCommand(ICommandSender sender, String[] args) {

        if (args.length == 1 && event.isBattleActive && sender instanceof EntityPlayer) { 
            EntityPlayerMP player = (EntityPlayerMP) sender;
    		 Scoreboard sb = getscoreboard();
            if (args[0].equals("attackers") && !event.ATTACKERS.contains(player) &&  !event.DEFENDERS.contains(player)) {
              	event.SIEGE.add(player);
            	event.ATTACKERS.add(player);

            	player.playerNetServerHandler.setPlayerLocation(AOSConfig.spawnAttackers[0], AOSConfig.spawnAttackers[1], AOSConfig.spawnAttackers[2], player.rotationYaw, player.rotationPitch);


            	sender.addChatMessage(new ChatComponentText("You joined the event!"));
                return;
            }
            if (args[0].equals("defenders") && !event.ATTACKERS.contains(player) &&  !event.DEFENDERS.contains(player)) {
              	event.SIEGE.add(player);
            	event.DEFENDERS.add(player);
            	player.playerNetServerHandler.setPlayerLocation(AOSConfig.spawnDefenders[0], AOSConfig.spawnDefenders[1], AOSConfig.spawnDefenders[2], player.rotationYaw, player.rotationPitch);
           

            	sender.addChatMessage(new ChatComponentText("You joined the event! "));

                return;
            }
            if (args[0].equals("leave") && event.ATTACKERS.contains(player)) {
              	event.SIEGE.remove(player);
            	event.ATTACKERS.remove(player);
            	sender.addChatMessage(new ChatComponentText("You left the event! "));
                return;
            }
            if (args[0].equals("leave" )&& event.DEFENDERS.contains(player)) {
              	event.SIEGE.remove(player);
            	event.DEFENDERS.remove(player);
            	sender.addChatMessage(new ChatComponentText("You left the event! "));
                return;
            }
        }
        if(args[0].equals("attackers") && event.ATTACKERS.contains(sender))
            throw new WrongUsageException("You are already in this side!", new Object[0]); 

        if(args[0].equals("defenders") && event.DEFENDERS.contains(sender))
            throw new WrongUsageException("You are already  this side!", new Object[0]); 
        
        if(args[0].equals("attackers") && event.DEFENDERS.contains(sender))
            throw new WrongUsageException("You are part of the other side!", new Object[0]); 

        if(args[0].equals("defenders") && event.ATTACKERS.contains(sender))
            throw new WrongUsageException("You are part of the other side!", new Object[0]); 
        
        if(args[0].equals("leave") && !event.SIEGE.contains(sender))
            throw new WrongUsageException("You already left!", new Object[0]); 
        
        if(args[0].equals("leave") && event.isBattleActive == false)
            throw new WrongUsageException("No battle is active!!", new Object[0]);   
        
        else {
        	throw new WrongUsageException("You need to do the rule quiz!");
        }
        
        
    }

    public List addTabCompletionOptions(ICommandSender sender, String[] args) {
        if (args.length == 1) {
            return CommandAddSiege.getListOfStringsMatchingLastWord((String[])args, (String[])new String[]{"attackers", "defenders", "leave"});
        }
        return null;
    }
}