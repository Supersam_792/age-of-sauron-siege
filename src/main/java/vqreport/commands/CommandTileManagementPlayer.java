package vqreport.commands;

import java.util.List;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import vqreport.core.AOSTileData;
import vqreport.core.TileEnum;

public class CommandTileManagementPlayer extends CommandBase {
	
	public String getCommandName() {
		return "tilecore";
	}

	@Override
	public String getCommandUsage(ICommandSender p_71518_1_) {
		return "/tilecore <faction> <player> <leader/officer>";
	}
	  @Override
	  public int getRequiredPermissionLevel() {
	      return 1;
	  }
	@Override
	public void processCommand(final ICommandSender sender,final  String[] args) {
        if (sender instanceof EntityPlayer) { 
	    TileEnum faction = TileEnum.forName(args[0]);
	    EntityPlayerMP player = CommandBase.getPlayer(sender, args[1]); 
	    AOSTileData data = AOSTileData.of(faction);
	        if (args.length >= 2 && args[2].equals("owner")) {

	        	data.setLeader(player.getUniqueID());
            	sender.addChatMessage(new ChatComponentText(player.getDisplayName() + " is now the owner of " + faction.name));

	        }
	        if(args[2].equals("remove")) {
		       data.setLeader(null);
           	sender.addChatMessage(new ChatComponentText(player.getDisplayName() + " is no longer the owner of " + faction.name));

	        }
        }
    }
	
	public List addTabCompletionOptions(final ICommandSender sender, final String[] args) {
        if (args.length == 1) {
        	 final List<String> list = TileEnum.getTiles();
             return getListOfStringsMatchingLastWord(args, (String[])list.toArray(new String[0]));
        }
        if (args.length == 2) {
            return getListOfStringsMatchingLastWord(args, MinecraftServer.getServer().getAllUsernames());
        }
        if (args.length == 3) {
            return getListOfStringsMatchingLastWord((String[])args, (String[])new String[]{"owner", "remove"});
        }
        return null;
    }
}