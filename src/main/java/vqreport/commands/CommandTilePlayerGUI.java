package vqreport.commands;

import lotr.common.LOTRMod;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.InventoryBasic;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.server.S2DPacketOpenWindow;
import net.minecraft.util.ChatComponentText;
import vqreport.core.AOSTileData;
import vqreport.core.TileEnum;
import vqreport.proxy.PlayerTileGUI;

public class CommandTilePlayerGUI extends CommandBase {
	
	public String getCommandName() {
		return "tileplayer";
	}

	@Override
	public String getCommandUsage(ICommandSender p_71518_1_) {
		return "/tilecore <faction> <player> <leader/officer>";
	}
	  @Override
	  public int getRequiredPermissionLevel() {
	      return 1;
	  }
	@Override
	public void processCommand(final ICommandSender sender,final  String[] args) {
        if (sender instanceof EntityPlayer) { 
        	EntityPlayerMP player = (EntityPlayerMP) sender;
        	if (player.openContainer != player.inventoryContainer) {
                player.closeScreen();
            }
            player.getNextWindowId();
            boolean isTrue = false;
            for(TileEnum enums : TileEnum.values()) {
            	AOSTileData data = AOSTileData.of(enums);
            	if(data.claimedTaxes != false);
            		isTrue = true;
            		break;
            }
          //  if(isTrue == false) {
	            final InventoryBasic chest = new InventoryBasic("Taxes", true, 9);
	            chest.setInventorySlotContents(0, new ItemStack(Blocks.wool, 1,5 ).setStackDisplayName("Claim with tax"));
	            chest.setInventorySlotContents(1, new ItemStack(Blocks.wool, 1,14).setStackDisplayName("Claim without tax"));
	
	            player.playerNetServerHandler.sendPacket(new S2DPacketOpenWindow(player.currentWindowId, 0, "Taxes", 9, true));
	            player.openContainer = new PlayerTileGUI(player.inventory, chest);
	            player.openContainer.windowId = player.currentWindowId;
	            player.openContainer.addCraftingToCrafters(player);
          /*  } else {
 	           sender.addChatMessage(new ChatComponentText("You already claimed taxes this period!"));
            }*/
            
        }
    }
}