package vqreport.commands;

import java.util.UUID;

import lotr.common.LOTRMod;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.InventoryBasic;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.server.S2DPacketOpenWindow;
import vqreport.core.AOSTileData;
import vqreport.core.TileEnum;
import vqreport.proxy.PlayerTileTaxesGUI;

public class CommandTilePlayerGUILeader extends CommandBase {
	
	public String getCommandName() {
		return "tileplayerleader";
	}

	@Override
	public String getCommandUsage(ICommandSender p_71518_1_) {
		return "/tilecore <faction> <player> <leader/officer>";
	}
	  @Override
	  public int getRequiredPermissionLevel() {
	      return 1;
	  }
	@Override
	public void processCommand(final ICommandSender sender,final  String[] args) {
        if (sender instanceof EntityPlayer) { 
        	EntityPlayerMP player = (EntityPlayerMP) sender;
        	if (player.openContainer != player.inventoryContainer) {
                player.closeScreen();
            }
            player.getNextWindowId();
            
           // EntityPlayer player = FMLClientHandler.instance().getClient().thePlayer;
            final InventoryBasic chest = new InventoryBasic("Leader Taxes", true, 54);
            int slotnumber = 0;
            for(TileEnum tiles : TileEnum.values()) {
            	AOSTileData data = AOSTileData.of(tiles);
            	for(UUID uuid : data.factionManagement) {
            		if(uuid != null) {
            			if(slotnumber >= 53) continue;
		                chest.setInventorySlotContents(slotnumber, new ItemStack(LOTRMod.bodyArnor).setStackDisplayName(tiles.name));
		                slotnumber++;
            			
            		}
            	}
            }

    //THIS OPENS THE GUI FOR THE PLAYER, TO GET THE EVENTS FOR WHEN THE PLAYER CLICKS AN ITEM IN THE CONTAINER YOU HAVE TO MAKE ANOTHER CLASS AND EXTEND ContainerChest. then you can use slotClick
            player.playerNetServerHandler.sendPacket(new S2DPacketOpenWindow(player.currentWindowId, 0, "Leader Taxes", 54, true));
            player.openContainer = new PlayerTileTaxesGUI(player.inventory, chest);
            player.openContainer.windowId = player.currentWindowId;
            player.openContainer.addCraftingToCrafters(player);
            
         //   player.openGui(AOS.MODID, CommonProxy.TILE_PLAYER, player.worldObj, player.serverPosX, player.serverPosY, player.serverPosZ);
            
        }
    }
}