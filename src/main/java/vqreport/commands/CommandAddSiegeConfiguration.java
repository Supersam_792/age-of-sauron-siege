package vqreport.commands;

import java.util.List;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.scoreboard.IScoreObjectiveCriteria;
import net.minecraft.scoreboard.ScoreObjective;
import net.minecraft.scoreboard.ScorePlayerTeam;
import net.minecraft.scoreboard.Scoreboard;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import vqreport.AOSConfig;
import vqreport.AOSEvents;

public class CommandAddSiegeConfiguration extends CommandBase
{
	public static AOSEvents event;
	private boolean battleactive = event.isBattleActive;


    public String getCommandName() {
        return "battleconf";
    }
    
    @Override
    public int getRequiredPermissionLevel() {
        return 1;
    }
    
    public String getCommandUsage(final ICommandSender sender) {
        return "commands.lotrta.loreadds.usage";
    }
    protected Scoreboard getscoreboard()
    {
        return MinecraftServer.getServer().worldServerForDimension(0).getScoreboard();
    }
    public void processCommand(ICommandSender sender, String[] args) {
        if (args.length >= 1 && sender instanceof EntityPlayer) {
            if (args[0].equals("enable") && !event.isBattleActive) {
            	event.isBattleActive = true;	
            	sender.addChatMessage(new ChatComponentText("You started the battle"));
                return;
        	}
            if (args[0].equals("disable") && event.isBattleActive) {
            	
            	event.isBattleActive = false;
            	for(Object playertje : MinecraftServer.getServer().getConfigurationManager().playerEntityList) {
            		EntityPlayer player = (EntityPlayer) playertje;
            		if(event.SIEGE.contains(player)) {
            			event.SIEGE.remove(player);
            			event.ATTACKERS.remove(player);
            			event.DEFENDERS.remove(player);
            			player.addChatMessage(new ChatComponentText("The event stopped!"));
            		}

            	}

            	event.SIEGE.clear();
            	event.ATTACKERS.clear();
            	event.DEFENDERS.clear();
            	sender.addChatMessage(new ChatComponentText("You ended the battle"));

                return;
                
                
     
            }
            if (args[0].equals("spawnAttackers")) {
            	int senderX = sender.getPlayerCoordinates().posX;
            	int senderY = sender.getPlayerCoordinates().posY;
            	int senderZ = sender.getPlayerCoordinates().posZ;
            	AOSConfig.spawnAttackers(senderX, senderY, senderZ);
                sender.addChatMessage(new ChatComponentText("spawnpoint attackers on: " + "X: "+ senderX + ", " + "Y:" + senderY + ", " + "Z:" + senderZ));
                return;
            }
            if (args[0].equals("spawnDefenders")) {
            	int senderX = sender.getPlayerCoordinates().posX;
            	int senderY = sender.getPlayerCoordinates().posY;
            	int senderZ = sender.getPlayerCoordinates().posZ;
            	AOSConfig.spawnDefenders(senderX, senderY, senderZ);
            	
                sender.addChatMessage(new ChatComponentText("spawnpoint defenders on: " + "X: "+ senderX + ", " + "Y:" + senderY + ", " + "Z:" + senderZ));
                return;
            }
            }
        
            if(args[0].equals("enable") && event.isBattleActive)
                throw new WrongUsageException("You already started a battle!", new Object[0]);
                        
            if(args[0].equals("disable") && !event.isBattleActive)
                throw new WrongUsageException("There is no battle right now!", new Object[0]); 
            
}
	public List addTabCompletionOptions(ICommandSender sender, String[] args) {
        if (args.length == 1) {
            return CommandAddSiegeConfiguration.getListOfStringsMatchingLastWord((String[])args, (String[])new String[]{"enable", "disable", "spawnAttackers", "spawnDefenders"});
        }
        return null;
    }
}