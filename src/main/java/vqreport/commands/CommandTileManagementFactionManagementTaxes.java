package vqreport.commands;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import vqreport.core.AOSTileData;
import vqreport.core.TileEnum;

public class CommandTileManagementFactionManagementTaxes extends CommandBase {
	
	public String getCommandName() {
		return "tilecoretaxes";
	}

	@Override
	public String getCommandUsage(ICommandSender p_71518_1_) {
		return "/tilecore <faction> <player> <leader/officer>";
	}
	  @Override
	  public int getRequiredPermissionLevel() {
	      return 1;
	  }
	@Override
	public void processCommand(final ICommandSender sender,final  String[] args) {
        if (sender instanceof EntityPlayer) { 
		    TileEnum faction = TileEnum.forName(args[0]);
	    	int amount = parseInt(sender, args[1]);
		    AOSTileData data = AOSTileData.of(faction);
		    EntityPlayer player = (EntityPlayer) sender;
	        //ITEM
		    if (args.length >= 2) {
		    	data.tax = amount;
		    	data.markDirty();
	           sender.addChatMessage(new ChatComponentText(player.getDisplayName() + " is now faction management of " + faction.name));
	
		        
		    }
        }
    }
    
    public List addTabCompletionOptions(final ICommandSender sender, final String[] args) {
        if (args.length == 1) {
       	 final List<String> list = TileEnum.getTiles();
            return getListOfStringsMatchingLastWord(args, (String[])list.toArray(new String[0]));
       }
       if (args.length == 2) {
           return getListOfStringsMatchingLastWord((String[])args, (String[])new String[]{"additem", "removeitem"});
       }
       if (args.length == 3) {
         	 final List<String> list = new ArrayList();
              return getListOfStringsMatchingLastWord(args, (String[])list.toArray(new String[0]));
         }
        return null;
    }
}