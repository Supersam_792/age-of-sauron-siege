package vqreport;

import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.common.config.Configuration;

public class AOSConfig {

	
	//HeadConfig
    public static Configuration config;
    private static List<String> allCategories;




	public static int[] spawnAttackers;
	public static int[] spawnDefenders;

    
    //Strings


    //More?
    public static void setupAndLoad(final FMLPreInitializationEvent event) {
    	config = new Configuration(event.getSuggestedConfigurationFile());
        load();
    }
    private static String getCategory(String category) {
        allCategories.add(category);
        return category;
    }

    public static void load() {
        /*Server Key*/

    	spawnAttackers = config.get(CATEGORY_GLOBAL, "spawnAttackers", new int[] { -31678, 109, -41510 }, "teleport to attacker spawn").getIntList();
    	spawnDefenders = config.get(CATEGORY_GLOBAL, "spawnDefenders", new int[] { -31678, 109, -41510 }, "teleport to defender spawn").getIntList();
        config.save();
    	
    }

	public static void spawnAttackers(int senderX, int senderY, int senderZ) {
		spawnAttackers[0] = senderX;
		spawnAttackers[1] = senderY;
		spawnAttackers[2] = senderZ;
		config.getCategory(CATEGORY_GLOBAL).get("spawnAttackers").set(spawnAttackers);
		config.save();
	}
	public static void spawnDefenders(int senderX, int senderY, int senderZ) {
		spawnDefenders[0] = senderX;
		spawnDefenders[1] = senderY;
		spawnDefenders[2] = senderZ;
		config.getCategory(CATEGORY_GLOBAL).get("spawnDefenders").set(spawnDefenders);
		config.save();
	}
    private static String CATEGORY_GLOBAL;

    static {
        allCategories = new ArrayList<String>();
        CATEGORY_GLOBAL = getCategory("global");
    }


    
}
