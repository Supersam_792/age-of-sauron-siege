package vqreport.core;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.item.ItemStack;

public enum TileEnum {

	TILE1(TileRewardsEnum.TIN),
	TILE2(TileRewardsEnum.TIN2),
	TILE3(TileRewardsEnum.TIN),
	TILE4(TileRewardsEnum.TIN);
	
	
	public String name;
	public TileRewardsEnum reward;
	TileEnum(TileRewardsEnum reward) {
		this.name = this.name();
		this.reward = reward;
		
	}
   public static TileEnum forName(String name) {
        for(TileEnum f : TileEnum.values()) {
            if(!f.matchesNameOrAlias(name)) continue;
            return f;
        }
        return null;
    }
   private boolean matchesNameOrAlias(String name) {
       if(this.name().equals(name)) {
           return true;
       }
       return false;
   }
   public static List<String> getTiles() {
       TileEnum[] factions = TileEnum.values();
       ArrayList<String> names = new ArrayList<String>();
       for(TileEnum f : factions) {
           names.add(f.name);
       }
       return names;
   }


}
