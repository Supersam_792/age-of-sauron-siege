package vqreport.core;

import java.util.ArrayList;

import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;

public enum TileRewardsEnum {

	TIN(new ItemStack(Blocks.gold_block, 63), new ItemStack(Blocks.diamond_block, 54)),
	TIN2(new ItemStack(Blocks.emerald_block, 18), new ItemStack(Blocks.dirt, 12));

	
	
	public String name;

	public ItemStack[] rewardlist;
	TileRewardsEnum(ItemStack... rewardList) {
		this.name = this.name();
		this.rewardlist = rewardList;
		
	}
}
