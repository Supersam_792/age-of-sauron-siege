package vqreport.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import javax.annotation.Nullable;

import com.google.common.collect.Maps;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import lotr.common.LOTRLevelData;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.common.util.Constants.NBT;
import vqreport.AOS;

public class AOSTileData {

	public static File ROOT;
	public static File ROOT_PD;
	public static void setRoots() {
		ROOT = folder(DimensionManager.getCurrentSaveRootDirectory(), AOS.MODID);
		ROOT_PD = folder(ROOT, "tiledata");
	}
	private static final Events events = new Events();
	public static final class Events {
		@SubscribeEvent
		public void onServerTick(TickEvent.ServerTickEvent event) {
			if(event.phase == TickEvent.Phase.END)
				for (TileEnum fac : TileEnum.values())of(fac).tick();	

					
			}					


	}
	static {
		setRoots();
		FMLCommonHandler.instance().bus().register(events);
		MinecraftForge.EVENT_BUS.register(events);
	}
	public static void runOnce() {
		for(TileEnum fac : TileEnum.values()) {
			if(fac == null) continue;
			AOSTileData TileData = AOSTileData.of(fac);
        	if(TileData.firstLoading == false) {
        		TileData.firstLoading = true;
        		TileData.claimedInfo = "unclaimed";
        		TileData.tax = 1;
        		TileData.writeToFile();
        	}

		}
		
	}
	private static final Map<TileEnum, AOSTileData> server_cache = Maps.newHashMap();

	private static File folder(File parent, String name) {
		File ret = new File(parent, name);
		if (!ret.exists()) {
			ret.mkdirs();
		}
		return ret;
	}

	public static AOSTileData of(TileEnum faction) {
		return server_cache.computeIfAbsent(faction, faction_ -> new AOSTileData(faction_).readFromFile());
	}

	public static void cleanAllButKeepThese(List<TileEnum> factionNames) {
		for (TileEnum key : new HashSet<TileEnum>(server_cache.keySet())) {
			if (!factionNames.contains(key)) {
				AOSTileData data = server_cache.remove(key);
				if (data == null)
					throw new RuntimeException("LOTRTA faction save data cache: Key was null! this should never happen!");
				data.writeToFile();
			}
		}
	}

	public static void saveAll() {
		server_cache.values().forEach(AOSTileData::writeToFile);
	}

	public static void quit() {
		saveAll();
		server_cache.clear();
	}

	public File file() {
		return new File(ROOT_PD, parent.name() + ".dat");
	}

	public AOSTileData readFromFile() {
		File file = file();
		if (file.exists()) {
			try {
				NBTTagCompound nbt = CompressedStreamTools.readCompressed(new FileInputStream(file));
				this.readFromNBT(nbt);
			} catch(Exception e) {
				e.printStackTrace();
			}
		} else {
			this.writeToFile();
		}
		return this;
	}

	public AOSTileData writeToFile() {
		File file = file();
		NBTTagCompound nbt = new NBTTagCompound();
		this.writeToNBT(nbt);
		try {
			CompressedStreamTools.writeCompressed(nbt, new FileOutputStream(file));
		} catch(Exception e) {
			e.printStackTrace();
		}
		return this;
	}
	private int ticksSinceSave = 1;

	public final void tick() {
		if (ticksSinceSave == 0) {
			writeToFile();

		/*	for(TileEnum factions : TileEnum.values())  {
				S2CFactionMoney packet = new S2CFactionMoney(money, factions);
				packet.sendToAll();
			}*/
			/*List thelist = net.minecraft.server.MinecraftServer.getServer().getConfigurationManager().playerEntityList;
			for (Object o : thelist) {
			EntityPlayer player = (EntityPlayer)o;
				final LOTRPlayerData clientPD = LOTRLevelData.getData(player);
				if (clientPD.getPledgeFaction() == this.parent) {
					  S2CFactionMoney packet = new S2CFactionMoney(money, clientPD.getPledgeFaction());
					  packet.sendTo((EntityPlayerMP) player);
				  }	
				}
			for(TileEnum factions : TileEnum.values())  {
			S2CFactionMoney packet = new S2CFactionMoney(money, factions);
			packet.sendToAll();
		}*/
			}	
		ticksSinceSave++;
		if (ticksSinceSave > 72000) {
			ticksSinceSave = -40;
		}
	}

	public final TileEnum parent;

	protected AOSTileData(TileEnum parent) {
		this.parent = parent;
	}

	public boolean firstLoading = false;
	public float bonus = 0.0f;
	public int tax = 0;
	@Nullable
	public UUID owner = null; // check
	@Nullable
	public List<UUID> factionManagement = new ArrayList<UUID>();
	@Nullable
	public List<ItemStack> rewardStacks = new ArrayList<ItemStack>();
	

	public boolean claimedTaxes = false;
	public String claimedInfo = "unclaimed";

	public final void writeToNBT(NBTTagCompound nbt) {
		
		nbt.setBoolean("firstLoading", firstLoading);
		writeNullableUUID(nbt, "owner", owner);
		final NBTTagList allyTag = new NBTTagList();
	    for (final UUID faction : factionManagement) {
	    	allyTag.appendTag(new NBTTagString(faction.toString()));
	    	
        	}
	    nbt.setTag("factionManagement", allyTag);
	    nbt.setFloat("bonus", bonus);
	    
	    //Rewards
	    final NBTTagList rewardTag = new NBTTagList();
	    for (final ItemStack item: rewardStacks) {
	        rewardTag.appendTag(item.writeToNBT(new NBTTagCompound()));
	    }
	    nbt.setTag("rewardStacks", rewardTag);
		nbt.setBoolean("claimedTaxes", claimedTaxes);
		nbt.setString("claimedInfo", claimedInfo);
	    nbt.setInteger("tax", tax);

	}

	public final void readFromNBT(NBTTagCompound nbt) {	
		firstLoading = nbt.getBoolean("firstLoading");
		owner = readNullableUUID(nbt, "owner");
		NBTTagList allyList = nbt.getTagList("factionManagement", Constants.NBT.TAG_STRING);
		factionManagement.clear();
		for(int i = 0; i < allyList.tagCount(); i++) {
		    String facName = allyList.getStringTagAt(i);
		    factionManagement.add(UUID.fromString(facName));

		}
		bonus = nbt.getFloat("bonus");
		
		//Rewards
		NBTTagList rewardList = nbt.getTagList("rewardStacks", Constants.NBT.TAG_COMPOUND);
		rewardStacks.clear();
		for(int i = 0; i < rewardList.tagCount(); i++) {
			rewardStacks.add(ItemStack.loadItemStackFromNBT(rewardList.getCompoundTagAt(i)));

		}
		claimedTaxes = nbt.getBoolean("claimedTaxes");
		claimedInfo = nbt.getString("claimedInfo");
		tax = nbt.getInteger("tax");

	}
	public static UUID readNullableUUID(NBTTagCompound nbt, String name) {
		if (!nbt.hasKey(name, NBT.TAG_STRING))
			return null;
		try {
			return UUID.fromString(nbt.getString(name));
		} catch(IllegalArgumentException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static void writeNullableUUID(NBTTagCompound nbt, String name, UUID uuid) {
		if (uuid != null)
			nbt.setString(name, uuid.toString());
	}
	public void setLeader(UUID profile) {
		if (!Objects.equals(owner, profile))
			markDirty();
			owner = profile;
	}
	public void addManagement(UUID uuid) {
		factionManagement.add(uuid);
		markDirty();
		
	}
	public void removeManagement(UUID uuid) {
		factionManagement.remove(uuid);
		markDirty();
		
	}

	public void addStack(ItemStack stack) {
		rewardStacks.add(stack);
		markDirty();
		
	}
	public void removeStack(ItemStack stack) {
		rewardStacks.remove(stack);
		markDirty();
		
	}
	// updaters and checkers -- all markDirty whenever the data changes

	public void markDirty() {
		if (ticksSinceSave > 0)
			ticksSinceSave = -40;
	}
	
}