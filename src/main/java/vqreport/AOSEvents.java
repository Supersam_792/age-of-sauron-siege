package vqreport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.PlayerRespawnEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.InventoryBasic;
import net.minecraft.scoreboard.Scoreboard;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.ChunkPosition;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.LivingDeathEvent;

public class AOSEvents {
	public static boolean isBattleActive;
	public static List<EntityPlayer> SIEGE = new ArrayList<>(); 
	public static List<EntityPlayer> ATTACKERS = new ArrayList<>(); 
	public static List<EntityPlayer> DEFENDERS = new ArrayList<>(); 
	public AOSEvents() {
		FMLCommonHandler.instance().bus().register(this);
		MinecraftForge.EVENT_BUS.register(this);
	}
    protected Scoreboard getscoreboard()  {
        return MinecraftServer.getServer().worldServerForDimension(0).getScoreboard();
    }
	@SubscribeEvent(priority = EventPriority.LOWEST)
	public void onPlayerRespawnBattle(final PlayerRespawnEvent event) {
	    if(!event.player.worldObj.isRemote && event.player instanceof EntityPlayer) {
   		 	Scoreboard sb = getscoreboard();

	        EntityPlayerMP player = (EntityPlayerMP) event.player;
			if(isBattleActive && ATTACKERS.contains(event.player)) {
				player.playerNetServerHandler.setPlayerLocation(AOSConfig.spawnAttackers[0], AOSConfig.spawnAttackers[1], AOSConfig.spawnAttackers[2], player.rotationYaw, player.rotationPitch);
			}
			if(isBattleActive && DEFENDERS.contains(event.player)) {
				player.playerNetServerHandler.setPlayerLocation(AOSConfig.spawnDefenders[0], AOSConfig.spawnDefenders[1], AOSConfig.spawnDefenders[2], player.rotationYaw, player.rotationPitch);
			}

		}
			
	}
	public static Map<EntityPlayerMP, InventoryBasic> toOpen = new HashMap<EntityPlayerMP, InventoryBasic>();

    @SubscribeEvent
    public void onPlayerTick(TickEvent.PlayerTickEvent event) {
        if (!toOpen.isEmpty() && toOpen.containsKey((EntityPlayerMP) event.player)) {
      //      A_Utils.displayGUIChest((EntityPlayerMP) event.player, toOpen.get(event.player));
            toOpen.remove((EntityPlayerMP) event.player);
            event.player.addChatComponentMessage(new ChatComponentText("Did it"));
      
        }
    }

                    
}
